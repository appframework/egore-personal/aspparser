package de.egore911.aspparser;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

@CommandLine.Command(name = "check-directory", mixinStandardHelpOptions = true)
public class CheckDirectory implements Callable<Integer> {

	private static final Logger LOG = LoggerFactory.getLogger(CheckDirectory.class);

	private static final Pattern p = Pattern.compile("<!--#INCLUDE (FILE|VIRTUAL)=\"(.*)\"-->",
			Pattern.CASE_INSENSITIVE);

	@CommandLine.Parameters(index = "0", description = "The directory to check for *.asp files")
	private String dir;
	@CommandLine.Parameters(index = "1", description = "The semicolor separated list of virtuals to resolve (e.g. 'namespace1,/var/asp/lib1;/lib2,/var/asp/lib2')", split = ";")
	private List<String> virtualsInput;
	@CommandLine.Parameters(index = "2", description = "The comma separated list of implicit variables/functions (e.g. 'name1,name2')", split = ",")
	private List<String> implicitsInput;

	private static String getFileContent(Path file, Map<String, Path> virtuals, Deque<String> inclusionStack) throws IOException {
		if (!Files.exists(file)) {
			String desiredFile = file.getFileName().toString().toLowerCase();
            try (DirectoryStream<Path> dir = Files.newDirectoryStream(file.toAbsolutePath().getParent())) {
                for (Path f : dir) {
                    if (f.getFileName().toString().toLowerCase().equals(desiredFile)) {
                        file = f;
                        break;
                    }
                }
            }
        }

		StringBuilder buffer = new StringBuilder();
		buffer.append("<!-- ");
		for (String s : inclusionStack) {
			buffer.append(s);
			buffer.append(" -> ");
		}
		buffer.append(file.getFileName().toString());
		inclusionStack.addLast(file.getFileName().toString());
		buffer.append(" -->\n");

		try (BufferedReader reader = Files.newBufferedReader(file,
				Charset.forName("ISO-8859-15"))) {
			String line;
			while ((line = reader.readLine()) != null) {
				buffer.append(line).append('\n');
			}
			String content = buffer.toString();

			Matcher matcher = p.matcher(content);
			StringBuilder sb = new StringBuilder();
			while (matcher.find()) {
				String type = matcher.group(1);
				String fileToInclude = matcher.group(2);
				Path includeFile = determineIncludeFile(file, virtuals, type, fileToInclude);
				String fileContentToInclude = getFileContent(includeFile, virtuals, inclusionStack);
				matcher.appendReplacement(sb, "");
				sb.append(fileContentToInclude);
			}
			matcher.appendTail(sb);

			sb.append("<!-- Done: ");
			inclusionStack.removeLast();
			for (String s : inclusionStack) {
				sb.append(s);
				sb.append(" -> ");
			}
			sb.append(file.getFileName().toString());
			sb.append(" -->\n");
			sb.append("<% %>"); // FIXME: remove this temporary workaround!

			content = sb.toString();
			return content;
		}
	}

	private static Path determineIncludeFile(Path file, Map<String, Path> virtuals, String type, String fileToInclude) {
		Path includeFile;
		if ("VIRTUAL".equalsIgnoreCase(type)) {
			includeFile = null;
			for (Map.Entry<String, Path> x : virtuals.entrySet()) {
				if (removeLeadingSlash(fileToInclude).startsWith(x.getKey())) {
					String replace = fileToInclude.replace(x.getKey(), "");
					replace = removeLeadingSlash(removeLeadingSlash(replace));
					includeFile = x.getValue().resolve(replace);
					break;
				}
			}
			if (includeFile == null) {
				throw new IllegalArgumentException("No valid virtual for " + fileToInclude);
			}
		} else {
			includeFile = file.resolveSibling(fileToInclude);
		}
		return includeFile;
	}

	public static void main(String[] args) {
		int exitCode = new CommandLine(new CheckDirectory()).execute(args);
		System.exit(exitCode);
	}

	@Override
	public Integer call() throws Exception {

		FileSystem fileSystem = FileSystems.getDefault();

		final Map<String, Path> virtuals;
		if (virtualsInput != null) {
			virtuals = new HashMap<>();
			for (String virtualInput : virtualsInput) {
				String[] v = virtualInput.split(",");
				Path path = fileSystem.getPath(v[1]).toAbsolutePath();
				if (!Files.exists(path)) {
					throw new IllegalArgumentException("The path " + path + " could not be found");
				}
				String namespace = removeLeadingSlash(v[0]);
				if (virtuals.put(namespace, path) != null) {
					throw new IllegalArgumentException("Virtual " + namespace + " assigned multiple times");
				}
			}
		} else {
			virtuals = Collections.emptyMap();
		}

		final Set<String> implicits = new HashSet<>();
		if (implicitsInput != null) {
			implicits.addAll(implicitsInput);
		}

		Path path = fileSystem.getPath(dir).toAbsolutePath();
		LOG.info("Analyzing all *.asp files below {}", path);
		Files.walkFileTree(path, new SimpleFileVisitor<>() {

			@Override
			public FileVisitResult visitFile(Path file,
											 BasicFileAttributes attrs) throws IOException {

				if (file.toString().toLowerCase(Locale.ENGLISH)
						.endsWith(".asp")) {

					LOG.info("Analyzing {}", file);

					String content = getFileContent(file, virtuals, new LinkedList<>());

					try (FileOutputStream fos = new FileOutputStream("tmp.asp")) {
						fos.write(content.getBytes());
					}

					CharStream charStream = CharStreams.fromString(content);
					AspLexer lexer = new AspLexer(charStream);
					List<String> lexerErrorMessages = new ArrayList<>();
					lexer.addErrorListener(new BaseErrorListener() {
						@Override
						public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
							lexerErrorMessages.add("line " + line + ":" + charPositionInLine + " " + msg);
						}
					});
					TokenStream tokenStream = new CommonTokenStream(lexer);
					AspParser parser = new AspParser(tokenStream);
					parser.getImplicitVariables().addAll(implicits);
					List<String> parserErrorMessages = new ArrayList<>();
					lexer.addErrorListener(new BaseErrorListener() {
						@Override
						public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
							parserErrorMessages.add("line " + line + ":" + charPositionInLine + " " + msg);
						}
					});
					try {
						parser.init();
						parser.parse();
					} catch (RecognitionException e) {
						LOG.error(e.getMessage(), e);
					}
					if (lexerErrorMessages.isEmpty() && parserErrorMessages.isEmpty()) {
						LOG.info("[ok]");
					} else {
						LOG.error("[failed]");
						for (String errorMessage : lexerErrorMessages) {
							LOG.error("Lexer: {}", errorMessage);
						}
						for (String errorMessage : parserErrorMessages) {
							LOG.error("Parser: {}", errorMessage);
						}
						return FileVisitResult.TERMINATE;
					}

				}

				return FileVisitResult.CONTINUE;
			}

		});

		return 0;
	}

	private static String removeLeadingSlash(String namespace) {
		if (namespace.startsWith("/")) {
			namespace = namespace.substring(1);
		}
		return namespace;
	}

}
