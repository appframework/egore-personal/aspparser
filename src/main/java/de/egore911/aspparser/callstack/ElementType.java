package de.egore911.aspparser.callstack;

public enum ElementType {
	CLASS, SUB, FUNCTION, VARIABLE, CONSTANT, PARAMETER, WITH
}
