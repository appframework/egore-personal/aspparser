package de.egore911.aspparser.callstack;

import org.antlr.v4.runtime.RecognitionException;

public class FileElement extends CallStackElement {

	public FileElement(String name, ElementType type, CallStackElement parent, RecognitionException e) {
		super(name, type, parent, e);
	}

	public FileElement(String name, ElementType type, CallStackElement parent, String occurrence) {
		super(name, type, parent, occurrence);
	}

	@Override
	public boolean isAllowedChild(ElementType type) {
		switch (type) {
		case CLASS:
		case CONSTANT:
		case FUNCTION:
		case SUB:
		case VARIABLE:
		case WITH:
			return true;
		case PARAMETER:
		default:
			return false;
		}
	}
}
