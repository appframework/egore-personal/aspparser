package de.egore911.aspparser.callstack;

import org.antlr.v4.runtime.RecognitionException;

public class VariableElement extends CallStackElement {

	private CallStackElement prototype;

	public VariableElement(String name, ElementType type, CallStackElement parent, RecognitionException e) {
		super(name, type, parent, e);
	}

	public VariableElement(String name, ElementType type, CallStackElement parent, String occurrence) {
		super(name, type, parent, occurrence);
	}

	@Override
	public boolean isAllowedChild(ElementType type) {
		switch (type) {
		case CONSTANT:
		case CLASS:
		case FUNCTION:
		case SUB:
		case VARIABLE:
		case PARAMETER:
		default:
			return false;
		}
	}

	@Override
	public CallStackElement isAllowedChild(String elementName) {
		CallStackElement child = super.isAllowedChild(elementName);
		if (child == null && prototype != null) {
			child = prototype.getChild(elementName);
		}
		return child;
	}

	public void setPrototype(CallStackElement prototype) {
		if (this.prototype != null) {
			// FIXME: What to do in this case? A variable changes it's type. It doesn't look safe to me
//			System.out.println(this.prototype + " vs " + prototype);
		}
		this.prototype = prototype;
	}
}
