package de.egore911.aspparser.callstack;

import org.antlr.v4.runtime.RecognitionException;

public class ClassElement extends CallStackElement {

	public ClassElement(String name, CallStackElement parent, RecognitionException e) {
		super(name, ElementType.CLASS, parent, e);
	}
	
	public ClassElement(String name, CallStackElement parent, String occurrence) {
		super(name, ElementType.CLASS, parent, occurrence);
	}

	@Override
	public boolean isAllowedChild(ElementType type) {
		switch (type) {
		case FUNCTION:
		case SUB:
		case VARIABLE:
			return true;
		case CLASS:
		case CONSTANT:
		case PARAMETER:
		case WITH:
		default:
			return false;
		}
	}
}
