package de.egore911.aspparser.callstack;

import org.antlr.v4.runtime.RecognitionException;

public class WithElement extends CallStackElement {

	public WithElement(String name, CallStackElement parent,
			RecognitionException e) {
		super(name, ElementType.WITH, parent, e);
	}

	public WithElement(String name, CallStackElement parent, String occurence) {
		super(name, ElementType.WITH, parent, occurence);
	}

	@Override
	public boolean isAllowedChild(ElementType type) {
		switch (type) {
		case VARIABLE:
			return true;
		case CONSTANT:
		case CLASS:
		case FUNCTION:
		case SUB:
		case PARAMETER:
		default:
			return false;
		}
	}

}
