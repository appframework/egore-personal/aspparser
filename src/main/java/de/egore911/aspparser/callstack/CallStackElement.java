package de.egore911.aspparser.callstack;

import java.util.HashMap;
import java.util.Map;

import org.antlr.v4.runtime.IntStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;

import de.egore911.aspparser.exceptions.DeclarationException;
import org.antlr.v4.runtime.Recognizer;

public abstract class CallStackElement {

	public String name;
	public ElementType type;
	private CallStackElement parent;
	public String occurrence;

	private Map<String, CallStackElement> children = new HashMap<>();

	public CallStackElement(String name, ElementType type, CallStackElement parent, RecognitionException e) {
		this.name = name;
		this.type = type;
		this.parent = parent;
		this.occurrence = "line " + e.getOffendingToken().getLine() + ":" + e.getOffendingToken().getCharPositionInLine();
	}

	public CallStackElement(String name, ElementType type, CallStackElement parent, String occurrence) {
		this.name = name;
		this.type = type;
		this.parent = parent;
		this.occurrence = occurrence;
	}

	public abstract boolean isAllowedChild(ElementType type);

	public void addChild(CallStackElement definitionOccurrence, Recognizer<?, ?> recognizer, IntStream input, ParserRuleContext ctx) throws RecognitionException {
		if (!isAllowedChild(definitionOccurrence.type)) {
			throw new DeclarationException(definitionOccurrence.type + " " + definitionOccurrence.name + " is not allowed to be a member of " + type, recognizer, input, ctx);
		}
		children.put(definitionOccurrence.name.toLowerCase(), definitionOccurrence);
	}

	public CallStackElement getChild(String name) {
		return children.get(name);
	}

	public CallStackElement getParent() {
		return parent;
	}

	@Override
	public String toString() {
		StringBuilder retval = new StringBuilder();
		if (parent != null) {
			retval.append(parent.toString());
			if (retval.length() > 0) {
				retval.append('.');
			}
		}
		retval.append(name);
		return retval.toString();
	}

	public CallStackElement isAllowedChild(String elementName) {
		return children.get(elementName.toLowerCase());
	}
}
