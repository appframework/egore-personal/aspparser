<%

dim i, x
for i = 1 to 7
	if i = 5 then
		exit for
	end if
	x = i
next

for i = 0 to 10 step 2
	x = i
next

for i = 10 to 0 step -2
	x = i
next

dim arr(2)
arr(0) = 1
arr(1) = 2
arr(2) = 3

dim z
for each z in arr
	x = z
next

i = 10
do while i > 3
	i = i - 1
loop

i = 10
do
	i = i -1
loop while i > 3

i = 0
do until i = 10
	i = i + 1
loop

i = 0
do
	i = i + 1
loop until i > 10

i = 0
do
	i = i + 1
	if i > 5 then
		exit do
	end if
loop until i > 10

i = 0
while not i = 1
	i = 1
wend

%>
