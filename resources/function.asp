<%

function f1()
	dim variable
	variable = 1
	f1 = variable
end function

dim x
x = f1()

sub f2(byval val, byref ref)
	ref = val
end sub

dim x1, x2
x1 = "Test"
call f2(x1, x2)

x1 = "test2"
f2 x1, x2

function f3(byref arr())
	arr(1) = "2"
	f3 = arr(1)
end function

dim a(1)
f3(a)

class abcClass
	sub b(arg1, arg2)
	end sub
	sub c(arg1, arg2, arg3, arg4)
	end sub
end class

dim abc
set abc = new abcClass

dim c1, d
' ASP offers a wide range of weird syntax
abc.b (c1 + 1), d

abc.c d,e,,f

sub b1(arg1, arg2, arg3, arg4, arg5, arg6)
end sub

b1(a, a, a,, a,_
	c)

%>
