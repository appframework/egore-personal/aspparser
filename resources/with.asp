<%

class classA
	public b, d, f
	public sub call1()
	end sub
	public sub call2(byval arg)
	end sub
end class

dim a
set a = new classA

with a
	.b = c
	.d = e
	.f = g()
end with

With b
	.b = c
	.call1
	.call2 c
	c = x(.b)
end with


with a
	call .b(c)
end with

%>
