<%

class EchoClass

	public function echo(byval var)
		response.write "This is a """ & var & """"
		echo = var
	end function

	public sub test(byval k)
		if k <> "1" then
			response.write k
		end if
		if request(k) <> "1" then
			response.write "1"
		end if
		if request("k") <> k then
			response.write "Test"
		end if
	end sub
end class

dim echo
set echo = new EchoClass

dim x
x = echo.echo("Hallo")
response.write(x)

%>
