<% dim x
x = 1
select case x
	case 1
		x = 2
	case 2
		x = "a"
	case else
		x = 1.2
end select

dim z
select case x(y):
	case a:
		z = 1
	case b:
		z = 2
end select

dim c
Select Case a:
	Case "1":	c = d
	Case Else:	c = b
End Select

sub b()
end sub

select case a
	case 1,2,3:
		b()
	case else: c()
end select

%>
