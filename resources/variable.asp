<%
	dim x
	x = b(c).d.e

	dim variable
	variable = "Test"

	Dim variable2
	variable2 = "Test"

	dim variable3, variable4
	variable3 = 1
	variable4 = true

	dim variable5(2)
	variable5(1) = "Test"
	variable5(2) = 2

	dim variable6(1,1)
	variable6(1,1) = "Test"

	dim variable7, & _
		variable8
	variable7 = "Test"
	variable8 = 3

	private variable9
	public variable10

	function c(arg1)
	end function

	variable9 = a.b c("x")

	variable9 = a.b(c("x"))(0)

	dim variable11
	variable11 = "test" & _
		"test"

	class bClass
		sub d(arg1, arg2)
		end sub
	end class

	class aClass
		dim b()
	end class

	dim a
	set a = new aClass
	set a.b = new bClass

	a.b(c).d "x", y

	dim m
	redim m(10)
	ReDim m(b(c).d.e - 1)

%>
