<%

dim y
if 1 = 2 then
	y = true
elseif 2 = 3 then
	y = true
elseif 3 = 4 then
	y = true
else
	y = false
end if

class aClass
	dim b
end class

dim a
set a = new aClass

dim x
if a.b = 0 then
	x = 1
else
	x = 2
end if

if x = 1 then
	a = a.b(src).b("cid")
else
	a = ""
end if

if x(y) then
	' Test
	response.end
end if

dim b, d
function c()
	if a = b then c = d
end function

if a = b then d = c()

sub die(arg)
	response.write arg
	response.end
end sub

die "Text " & c & " more text"

sub d1()
	if a = b then
		die "Text " & c & " more text"
	end if
	if a = b then die "Text " & c & " more text"
	if a = b then b = a else a = b
End Sub

if a <= &b then
	a = c
end if

if a = 1 then a = 2 end if

if b = c then
	if c = d then b()
	a()
else
	a()
end if

if a > 1 then %>
a
<% else %>
b
<% end if %>
<%

%>
